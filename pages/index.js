import React from 'react'

import { connect } from 'react-redux'

import { startClock } from '../src/clock/actions'
import { countIncrease } from '../src/count/actions'
import { loadData } from '../src/placeholder/actions'
import { setSurveyData } from '../src/survey/actions'

import App from '../src/components/App'
import Header from '../src/components/Header'
import Page from '../src/components/Page'

class PageIndex extends React.Component {
  static async getInitialProps({ ctx: { store } }) {
    store.dispatch(countIncrease())

    if (!store.getState().placeholder.data) {
      store.dispatch(loadData())
    }

    if (!store.getState().survey.data) {
      store.dispatch(setSurveyData())
    }
  }

  componentDidMount() {
    this.props.dispatch(startClock())
  }

  render() {
    return (
      <App>
        <Header />
        <Page title="Home Page" />
      </App>
    )
  }
}

export default connect()(PageIndex)
