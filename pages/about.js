import App from '../src/components/App'
import Header from '../src/components/Header'

export default () => (
  <App>
    <Header />
    <article>
      <h1>How Apollo loads data</h1>
      <p>
        On initial page load, while on the server and inside getInitialProps, we
        invoke the Apollo method,{' '}
        <a href="https://www.apollographql.com/docs/react/features/server-side-rendering.html#getDataFromTree">
          getDataFromTree
        </a>
        . This method returns a promise; at the point in which the promise
        resolves, our Apollo Client store is completely initialized.
      </p>
    </article>
  </App>
)
