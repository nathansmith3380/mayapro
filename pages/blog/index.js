import React from 'react'

import withApollo from '../../src/withApollo'
import withReduxSaga from '../../src/withReduxSaga'

import { startClock } from '../../src/clock/actions'

import App from '../../src/components/App'
import Header from '../../src/components/Header'
import Submit from '../../src/blog/components/Submit'
import PostList from '../../src/blog/components/PostList'

class BlogIndex extends React.Component {
  componentDidMount() {
    this.props.store.dispatch(startClock())
  }

  render() {
    return (
      <App>
        <Header />
        <Submit />
        <PostList />
      </App>
    )
  }
};

export default withReduxSaga(withApollo(BlogIndex));
