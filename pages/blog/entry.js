import withApollo from '../../src/withApollo'

import App from '../../src/components/App'
import Header from '../../src/components/Header'
import Post from '../../src/blog/components/Post'

export default withApollo(() => (
  <App>
    <Header />
    <Post />
  </App> 
));
