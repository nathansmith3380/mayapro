import React from 'react'

import withApollo from '../../src/withApollo'
import withReduxSaga from '../../src/withReduxSaga'

import App from '../../src/components/App'
import Header from '../../src/components/Header'
import Survey from '../../src/survey/components/Survey'

class TakeSurvey extends React.Component {
  render() {
    return (
      <App>
        <Header />
        <Survey />
      </App>
    )
  }
};

export default withReduxSaga(withApollo(TakeSurvey));
