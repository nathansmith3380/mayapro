import Link from 'next/link'
import { withRouter } from 'next/router'
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavItem } from "mdbreact";

import "@fortawesome/fontawesome-free/css/all.min.css";
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

const Header = ({ router: { pathname } }) => (
  <header>
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@0.5.10/dist/css/bootstrap-material-design.css"></link>
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@0.5.10/dist/css/ripples.css"></link>

    <MDBNavbar color="indigo" dark expand="md">
      <MDBNavbarBrand>
        <strong className="white-text">MayaHealth</strong>
      </MDBNavbarBrand>
      <MDBNavbarNav left>
        <MDBNavItem>
          <Link href='/'>
            <a className={`nav-link ${pathname === '/' ? 'is-active' : ''}`}>Home</a>
          </Link>
        </MDBNavItem>
        <MDBNavItem>
          <Link href='/blog'>
            <a className={`nav-link ${pathname === '/blog' ? 'is-active' : ''}`}>Blog Posts</a>
          </Link>
        </MDBNavItem>
        <MDBNavItem>
          <Link href='/survey'>
            <a className={`nav-link ${pathname === '/survey' ? 'is-active' : ''}`}>Survey</a>
          </Link>
        </MDBNavItem>
        <MDBNavItem>
          <Link href='/about'>
            <a className={`nav-link ${pathname === '/about' ? 'is-active' : ''}`}>About</a>
          </Link>
        </MDBNavItem>
      </MDBNavbarNav>
    </MDBNavbar>
        
    <style jsx>{`
      header {
        margin-bottom: 25px;
      }
      a {
        font-size: 14px;
        margin-right: 15px;
        text-decoration: none;
      }
      .is-active {
        text-decoration: underline;
      }
    `}</style>
  </header>
)

export default withRouter(Header)
