import { actionTypes } from './actions'

export const initialState = {
  data: {},
  isComplete: false
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.SET_DATA:
      return {
        ...state,
        ...{ data: action.data },
      };

    case actionTypes.SET_COMPLETE_FLAG:
      return {
        ...state,
        ...{ isComplete: action.data }
      };

    default:
      return state;
  }
}

export default reducer;
