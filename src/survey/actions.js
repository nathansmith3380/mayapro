export const actionTypes = {
  SET_DATA: 'survey/SET_DATA',
  SET_COMPLETE_FLAG: 'survey/SET_COMPLETE_FLAG'
};

export function setSurveyData(data) {
  return {
    type: actionTypes.SET_DATA,
    data
  };
};

export function setSurveyCompleteFlag(isComplete) {
  return {
    type: actionTypes.SET_COMPLETE_FLAG,
    isComplete
  };
};
