#!/bin/bash
set -e

CLEAN_BRANCH_NAME=${CIRCLE_BRANCH//\//-};

JSON=$(cat <<-EOF
{
    "name": "maya-$CLEAN_BRANCH_NAME",
    "alias": ["maya-health-web-feature.now.sh"]
}
EOF
)

# Preferably, would like to use:
# "name": "$CIRCLE_PROJECT_REPONAME-$CLEAN_BRANCH_NAME",
# "alias": ["$CLEAN_BRANCH_NAME-circleci-now.now.sh"]

echo $JSON > .now/now.feature.json